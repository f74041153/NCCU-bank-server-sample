const express = require('express');
const router = express.Router();

const Web3 = require('web3');

//const web3 = new Web3('http://localhost:8545'); 

// Using the IPC provider in node.js
const net = require('net');
//const web3 = new Web3('\\\\.\\pipe\\geth.ipc', net); // on windows

//const web3 = new Web3('/Users/myuser/Library/Ethereum/geth.ipc', net); // mac os path
//const web3 = new Web3('/users/myuser/.ethereum/geth.ipc', net); // on linux
const web3 = new Web3();
const ganache = require("ganache-cli");
web3.setProvider(ganache.provider());

const contract = require('../contract/Bank.json');

/* GET home page. */
router.get('/', async function (req, res, next) {
  res.render('index')
});

//get accounts
router.get('/accounts', async function (req, res, next) {
  let accounts = await web3.eth.getAccounts()
  res.send(accounts)
});

//mintCoin
router.post('/mintCoin',async function(req,res,next){
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.mintCoin(req.body.value).send({
    from: req.body.account,
    gas: 3400000
  })
  .on('receipt', function (receipt) {
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

//buyCoin
router.post('/buyCoin',async function(req,res,next){
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.buyCoin().send({
    from: req.body.account,
    gas: 3400000,
    value: req.body.value
  })
  .on('receipt', function (receipt) {
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

//transferCoin
router.post('/transferCoin',async function(req,res,next){

  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.transferCoin(req.body.to, req.body.value).send({
    from: req.body.account,
    gas: 3400000
  })
  .on('receipt', function (receipt) {
    console.log(receipt);
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

router.post('/owner', async function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  let owner = await bank.methods.getOwner().call({ from: req.body.account });
  res.send({
    owner: owner
  })
});

router.post('/transferOwner',async function(req,res,next){

  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.transferOwner(req.body.to).send({
    from: req.body.account,
    gas: 3400000
  })
  .on('receipt', function (receipt) {
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

//buyStock
router.post('/buyStock',async function(req,res,next){
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.buyStock(req.body.period).send({
    from: req.body.account,
    gas: 3400000,
    value: req.body.capital
  })
  .on('receipt', function (receipt) {
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

//buyStock
router.post('/returnStock',async function(req,res,next){
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.returnStock(req.body.period).send({
    from: req.body.account,
    gas: 3400000,
  })
  .on('receipt', function (receipt) {
    res.send(receipt);
  })
  .on('error', function (error) {
    res.send(error.toString());
  })
})

//login
router.get('/balance', async function (req, res, next) {
  let ethBalance = await web3.eth.getBalance(req.query.account)
  res.send({
    ethBalance: ethBalance
  })
});

//balance
router.get('/allBalance', async function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.query.address;
  let ethBalance = await web3.eth.getBalance(req.query.account)
  let bankBalance = await bank.methods.getBankBalance().call({ from: req.query.account })
  let coinBalance = await bank.methods.getCoinBalance().call({ from: req.query.account })
  res.send({
    ethBalance: ethBalance,
    bankBalance: bankBalance,
    coinBalance: coinBalance
  })
});

//contract
router.get('/contract', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.query.address;
  res.send({
    bank: bank
  })
});

//unlock account
router.post('/unlock', function (req, res, next) {
  web3.eth.personal.unlockAccount(req.body.account, req.body.password, 60)
    .then(function (result) {
      res.send('true')
    })
    .catch(function (err) {
      res.send('false')
    })
});

//deploy bank contract
router.post('/deploy', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.deploy({
    data: contract.bytecode
  })
    .send({
      from: req.body.account,
      gas: 3400000,
      value: web3.utils.toWei('10', 'ether')
    })
    .on('receipt', function (receipt) {
      res.send(receipt);
    })
    .on('error', function (error) {
      res.send(error.toString());
    })
});

//deposit ether
router.post('/deposit', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.deposit().send({
    from: req.body.account,
    gas: 3400000,
    value: web3.utils.toWei(req.body.value, 'ether')
  })
    .on('receipt', function (receipt) {
      res.send(receipt);
    })
    .on('error', function (error) {
      res.send(error.toString());
    })
});

//withdraw ether
router.post('/withdraw', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.withdraw(req.body.value).send({
    from: req.body.account,
    gas: 3400000
  })
    .on('receipt', function (receipt) {
      res.send(receipt);
    })
    .on('error', function (error) {
      res.send(error.toString());
    })
});

//transfer ether
router.post('/transfer', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.transfer(req.body.to, req.body.value).send({
    from: req.body.account,
    gas: 3400000
  })
    .on('receipt', function (receipt) {
      res.send(receipt);
    })
    .on('error', function (error) {
      res.send(error.toString());
    })
});

//kill contract
router.post('/kill', function (req, res, next) {
  let bank = new web3.eth.Contract(contract.abi);
  bank.options.address = req.body.address;
  bank.methods.kill().send({
    from: req.body.account,
    gas: 3400000
  })
    .on('receipt', function (receipt) {
      res.send(receipt);
    })
    .on('error', function (error) {
      res.send(error.toString());
    })
});

module.exports = router;
