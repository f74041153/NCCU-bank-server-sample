pragma solidity ^0.4.23;

contract Bank {
	// 此合約的擁有者
    address private owner;

	// 儲存所有會員的餘額
    mapping (address => uint256) private balance;

	// 事件們，用於通知前端 web3.js
    event DepositEvent(address indexed from, uint256 value, uint256 timestamp);
    event WithdrawEvent(address indexed from, uint256 value, uint256 timestamp);
    event TransferEvent(address indexed from, address indexed to, uint256 value, uint256 timestamp);
    event MintCoinEvent(uint minted);
    event BuyCoinEvent(uint rtn);
    event TransferCoinEvent(uint rtn);
    event TransferOwnerEvent(address owner);
    event BuyStockEvent (uint rtn);
    event ReturnStockEvent (uint rtn);
    
    modifier isOwner() {
        require(owner == msg.sender, "you are not owner");
        _;
    }
    
	// 建構子
    constructor() public payable {
        owner = msg.sender;
    }

	// 存錢
    function deposit() public payable {
        balance[msg.sender] += msg.value;

        emit DepositEvent(msg.sender, msg.value, now);
    }

	// 提錢
    function withdraw(uint256 etherValue) public {
        uint256 weiValue = etherValue * 1 ether;

        require(balance[msg.sender] >= weiValue, "your balances are not enough");

        msg.sender.transfer(weiValue);

        balance[msg.sender] -= weiValue;

        emit WithdrawEvent(msg.sender, etherValue, now);
    }

	// 轉帳
    function transfer(address to, uint256 etherValue) public {
        uint256 weiValue = etherValue * 1 ether;

        require(balance[msg.sender] >= weiValue, "your balances are not enough");

        balance[msg.sender] -= weiValue;
        balance[to] += weiValue;

        emit TransferEvent(msg.sender, to, etherValue, now);
    }

	// 檢查銀行帳戶餘額
    function getBankBalance() public view returns (uint256) {
        return balance[msg.sender];
    }

    function kill() public isOwner {
        selfdestruct(owner);
    }

    //只能由合約的Owner使用，並指定下一個Owner
    function transferOwner(address _newOwner) public isOwner{
        owner = _newOwner;
        emit TransferOwnerEvent(owner);
    }

    function getOwner() public view returns (address){
        return owner;
    }


    //1期的利率為0.01，一個帳戶在一個合約中只能擁有一個定存

    
    mapping (address=>uint) private myCoinBalance;
   
    //合約的Owner可以鑄造新的COIN
    function mintCoin(uint _amount) public isOwner{
        myCoinBalance[owner]+=_amount;
        emit MintCoinEvent(_amount);
    }

    //其他帳戶可以向Owner購買COIN,幣值1:1?
    function buyCoin() payable public{
        require(owner != msg.sender,"You are owner cannot buy your coin");
        owner.transfer(msg.value);
        myCoinBalance[msg.sender]+=msg.value;
        myCoinBalance[owner]-=msg.value;
        emit BuyCoinEvent(myCoinBalance[owner]);
    }

    //帳戶之間可以轉帳COIN
    function transferCoin(address _to, uint _amount) public {
        myCoinBalance[msg.sender]-=_amount;
        myCoinBalance[_to]+=_amount;
        emit TransferCoinEvent(myCoinBalance[_to]);
    }

    function getCoinBalance() public view returns (uint256) {
        return myCoinBalance[msg.sender];
    }

    struct stock{
        uint capital;
        uint period;
        bool paid;
    }
    mapping (address => stock) stock_data;

    function buyStock (uint _period) public payable{
        stock_data[msg.sender].capital = msg.value;
        stock_data[msg.sender].period = _period;
        stock_data[msg.sender].paid= false;
        emit BuyStockEvent(msg.value);
    }

    function returnStock (uint _finished) public {
        require(!stock_data[msg.sender].paid);
        require(stock_data[msg.sender].period >= _finished);
        uint payback = stock_data[msg.sender].capital+(stock_data[msg.sender].capital*_finished)/100;
        msg.sender.transfer(payback);
        stock_data[msg.sender].paid = true;
        emit ReturnStockEvent(payback);
    }
    
}